package swagger

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"

	"log"

	"github.com/go-openapi/analysis"
	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/runtime/middleware/untyped"
	"github.com/go-openapi/spec"
	"github.com/go-openapi/strfmt"
)

type routableUntypedAPI struct {
	api             *untyped.API
	hlock           *sync.Mutex
	spec            *loads.Document
	handlers        map[string]map[string]http.Handler
	defaultConsumes string
	defaultProduces string
}

func newSecureAPI(ctx *middleware.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		route, _ := ctx.RouteInfo(r)
		if route != nil && len(route.Authenticators) == 0 {
			next.ServeHTTP(rw, r)
			return
		}

		if _, err := ctx.Authorize(r, route); err != nil {
			ctx.Respond(rw, r, route.Produces, route, err)
			return
		}

		next.ServeHTTP(rw, r)
	})
}
func (r *routableUntypedAPI) HandlerFor(method, path string) (http.Handler, bool) {
	r.hlock.Lock()
	paths, ok := r.handlers[strings.ToUpper(method)]
	if !ok {
		r.hlock.Unlock()
		return nil, false
	}
	handler, ok := paths[path]
	r.hlock.Unlock()
	return handler, ok
}
func (r *routableUntypedAPI) ServeErrorFor(operationID string) func(http.ResponseWriter, *http.Request, error) {
	return r.api.ServeError
}
func (r *routableUntypedAPI) ConsumersFor(mediaTypes []string) map[string]runtime.Consumer {
	return r.api.ConsumersFor(mediaTypes)
}
func (r *routableUntypedAPI) ProducersFor(mediaTypes []string) map[string]runtime.Producer {
	return r.api.ProducersFor(mediaTypes)
}
func (r *routableUntypedAPI) AuthenticatorsFor(schemes map[string]spec.SecurityScheme) map[string]runtime.Authenticator {
	return r.api.AuthenticatorsFor(schemes)
}
func (r *routableUntypedAPI) Formats() strfmt.Registry {
	return r.api.Formats()
}

func (r *routableUntypedAPI) DefaultProduces() string {
	return r.defaultProduces
}

func (r *routableUntypedAPI) DefaultConsumes() string {
	return r.defaultConsumes
}

func (r *routableUntypedAPI) setupHandlers(proxyHandler http.Handler, context *middleware.Context) {
	analyzer := analysis.New(r.spec.Spec())
	for method, hls := range analyzer.Operations() {
		um := strings.ToUpper(method)
		for path, op := range hls {
			schemes := analyzer.SecurityDefinitionsFor(op)

			if r.handlers == nil {
				r.handlers = make(map[string]map[string]http.Handler)
			}
			if b, ok := r.handlers[um]; !ok || b == nil {
				r.handlers[um] = make(map[string]http.Handler)
			}

			var handler http.Handler = http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
				// lookup route info in the context
				route, _ := context.RouteInfo(req)

				buf, err := ioutil.ReadAll(req.Body)
				if err != nil {
					log.Fatal(err)
				}
				rdr1 := ioutil.NopCloser(bytes.NewBuffer(buf))
				rdr2 := ioutil.NopCloser(bytes.NewBuffer(buf))

				req.Body = rdr1
				// bind and validate the request using reflection
				_, validation := context.BindAndValidate(req, route)
				if validation != nil {
					context.Respond(w, req, route.Produces, route, validation)
					return
				}
				req.Body = rdr2
				proxyHandler.ServeHTTP(w, req)
				//if err != nil {
				//	// respond with failure
				//	context.Respond(w, r, route.Produces, route, err)
				//	return
				//}
				//
				//// respond with success
				//context.Respond(w, r, route.Produces, route, result)
			})

			if len(schemes) > 0 {
				handler = newSecureAPI(context, handler)
			}
			r.handlers[um][path] = handler
		}
	}
}
